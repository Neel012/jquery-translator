let input = $('input[name=translate-input');
let output = $('input[name=translate-output');

for (let i = 0; i < localStorage.length; ++i) {
    let key = localStorage.key(i);
    $('#history').append("<tr><td>" + key + "</td><td>" + localStorage[key] + "</td></tr>");
}

$('button[name=translate-button').on("click", function (event) {
    var url = "https://api.mymemory.translated.net/get?langpair=en|cs&q=" + input.val();
    $.getJSON(url, function (response) {
        output.val(response.matches[0].translation);
        localStorage.setItem(input.val(), output.val());
        $('#history').prepend("<tr><td>" + input.val() + "</td><td>" + output.val() + "</td></tr>");
    });
});
